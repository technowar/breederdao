# breederdao

## Instructions

1. Create an API Endpoint using Nodejs, GraphQL and MongoDb
2. Create a collections to Mongodb for each Axie Class

  Collection Names:
  * aquatic_class
  * beast_class
  * bird_class
  * bug_class
  * plant_class
  * reptile_class

3. Fetch the first 300 Axies (sorted by price ascending) from Axie API and get its id, name, stage, class, and, current price in USD ($)
4. Group the results by Axie Class (Aquatic, Beast, Bird, Bug, Plant, Reptile) and save to mongodb collection based on their classification (axie class).

You can use this https://github.com/ShaneMaglangit/axie-graphql-documentation/blob/main/src/docs/operations/getAxieBriefList.mdx as reference for the Axie API

## Prerequisites

* Node@14.x or greater
* MongoDB

## Caveats

* Maximum of 100 Axies per query
* Axies with classes not part of the list are stored in `Others` collection
* No tests, at all

## Queries

* Fetch all
```
query {
  getAll {
    id
    class
    name
    price
    stage
  }
}
```

* Fetch by class
```
query {
  getAllByClass(
    data: {
      className: Aquatic | Beast | Bird | Bug | Others | Plant | Reptile, defaults to Aquatic
    }
  ) {
    id
    class
    name
    price
    stage
  }
}
```

* Seed
```
mutation {
  seed(
    data: {
      auctionType: All | Sale | NotForSale, defaults to All
      size: 5, defaults to 3
      sort: PriceAsc | PriceDesc, defaults to PriceAsc
    }
  ) {
    id
    class
    name
    price
    stage
  }
}
```
