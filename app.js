"use strict";

const Fs = require("fs");
const Path = require("path");
const Express = require("express");
const { ApolloServer } = require("apollo-server-express");

require("dotenv").config();

exports.Server = async () => {
  // Load models
  const Models = Path.join(__dirname, "app/models");

  Fs.readdirSync(Models)
    .filter(Model => ~Model.search(/^[^\.].*\.js$/))
    .forEach(Model => require(Path.join(Models, Model)));

  const App = Express();

  // Load GraphQL
  const GraphConfig = require("./app/config/GraphConfig");
  const Apollo = new ApolloServer({
    typeDefs: GraphConfig.typeDefs,
    resolvers: GraphConfig.resolvers,
    context: req => ({ req })
  });

  await Apollo.start();

  Apollo.applyMiddleware({ app: App });

  // Load routes
  require('./app/config/RouteConfig')(App);

  return App;
};
