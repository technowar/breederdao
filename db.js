"use strict";

const Mongoose = require("mongoose");

exports.Connection = async () => {
  const MongoURL = process.env.MONGO_URL || "mongodb://127.0.0.1/breeder";

  return await Mongoose.connect(MongoURL, {
    connectTimeoutMS: 10000,
    socketTimeoutMS: 45000,
    useNewUrlParser: true,
  });
};
