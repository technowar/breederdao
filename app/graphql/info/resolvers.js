"use strict";

const {
  GetAll,
  GetAllByClass,
  InsertAll,
  Seed
} = require("./info");

module.exports = {
  Mutation: {
    seed: async (_, value) => {
      const { data: variables } = value;
      const axies = await Seed(variables);

      return await InsertAll(axies);
    }
  },
  Query: {
    getAll: async () => await GetAll(),
    getAllByClass: async (_, value) => {
      const {
        data: { className }
      } = value;

      return await GetAllByClass(className);
    }
  }
};
