"use strict";

const { gql } = require("apollo-server-express");

module.exports = gql`
  type Info {
    id: ID
    axieId: String
    class: String
    name: String
    price: String
    stage: Int
  }

  input SeedPayload {
    auctionType: AuctionType = All
    size: Int = 3
    sort: SortType = PriceAsc
  }

  input GetByClassPayload {
    className: ClassNameType = Aquatic
  }

  enum AuctionType {
    All
    Sale
    NotForSale
  }

  enum ClassNameType {
    Aquatic
    Beast
    Bird
    Bug
    Others
    Plant
    Reptile
  }

  enum SortType {
    PriceAsc
    PriceDesc
  }

  extend type Mutation {
    seed(data: SeedPayload!): [Info]
  }

  extend type Query {
    getAll: [Info]
    getAllByClass(data: GetByClassPayload): [Info]
  }
`;
