"use strict";

const { Classes, Collections } = require("../../utils/collection");
const { FlatAndSort, InArray } = require("../../utils/helpers");
const { Request } = require("../../utils/service");

module.exports = {
  GetAll: async () => {
    try {
      const axies = await Promise.all(Classes.map(async className => {
        const collectionName = await Collections(className);

        return await collectionName.find().exec();
      }));

      return FlatAndSort(axies, "price");
    } catch (error) {
      console.error("[GETALL] Unhandled error:", error.message);

      return [];
    }
  },
  GetAllByClass: async className => {
    try {
      const collectionName = await Collections(className);

      return await collectionName.find().sort("field price").exec();
    } catch (error) {
      console.error("[GETALLBYCLASS] Unhandled error:", error.message);

      return [];
    }
  },
  Insert: async item => {
    try {
      const collectionName = await Collections(item.class);

      return await collectionName.create({
        axieId: item.axieId,
        class: item.class,
        name: item.name,
        price: item.auction?.price || '0.0',
        stage: item.stage
      });;
    } catch (error) {
      console.error("[INSERT] Unhandled error:", error.message);

      return {};
    }
  },
  InsertAll: async items => {
    try {
      const list = items.reduce((acc, curr) => {
        const key = InArray(Classes, curr.class) ? curr.class : "Others";

        (acc[key] = acc[key] || []).push({
          axieId: curr.axieId,
          class: curr.class,
          name: curr.name,
          price: curr.auction?.price || '0.0',
          stage: curr.stage
        });

        return acc;
      }, {});

      const axies = await Promise.all(Object.keys(list).map(async item => {
        const collectionName = await Collections(item);

        return await collectionName.insertMany(list[item], {
          ordered: false
        });
      }));

      return FlatAndSort(axies, "price");
    } catch (error) {
      console.error("[INSERTALL] Unhandled error:", error.message);

      return await module.exports.GetAll();
    }
  },
  Seed: async variables => {
    try {
      const payload = JSON.stringify({
        query: `
          query GetAxieBriefList(
            $auctionType: AuctionType,
            $size: Int,
            $sort: SortBy
          ) {
            axies(
              auctionType: $auctionType,
              size: $size,
              sort: $sort
            ) {
              results {
                axieId: id
                class
                name
                stage
                auction {
                  price: currentPriceUSD
                }
              }
            }
          }
        `,
        variables
      });
      const {
        data: {
          axies: { results }
        }
      } = await Request(payload);

      return results;
    } catch (error) {
      console.error("[SEED] Unhandled error:", error.message);

      return await module.exports.GetAll();
    }
  }
};
