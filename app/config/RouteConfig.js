"use strict";

const { Landing } = require("../routes/Axie");

module.exports = (App) => {
  App.get("/", Landing);
}
