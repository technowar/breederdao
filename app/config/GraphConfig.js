"use strict";

const { gql } = require("apollo-server-express");
const merge = require("lodash.merge");
const Info = require("../graphql/info");

const typeDef = gql`
  type Query
  type Mutation
`;

module.exports = {
  typeDefs: [
    typeDef,
    Info.typeDefs
  ],
  resolvers: merge(
    Info.resolvers
  )
};
