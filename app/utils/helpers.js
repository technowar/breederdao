"use strict";

module.exports = {
  FlatAndSort: (list, sort) => list.flat().sort((a, b) => a[sort] > b[sort] && 1 || -1),
  InArray: (list, val) => list.some(item => item === val)
};
