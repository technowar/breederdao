"use strict";

const https = require("https");

module.exports = {
  Request: payload => {
    return new Promise((resolve, reject) => {
      const req = https.request({
        hostname: "graphql-gateway.axieinfinity.com",
        port: 443,
        path: "/graphql",
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "Content-Length": payload.length,
          "User-Agent": "Node"
        },
      }, res => {
        res.setEncoding("utf8");

        let body = "";

        res.on("data", chunk => {
          body += chunk;
        });

        res.on("end", () => {
          resolve(JSON.parse(body));
        });
      });

      req.on("error", err => {
        reject(err);
      });

      req.write(payload)
      req.end();
    });
  }
};
