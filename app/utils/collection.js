"use strict";

const Aquatic = require("../models/Aquatic");
const Beast = require("../models/Beast");
const Bird = require("../models/Bird");
const Bug = require("../models/Bug");
const Others = require("../models/Others");
const Plant = require("../models/Plant");
const Reptile = require("../models/Reptile");

module.exports = {
  Classes: [
    "Aquatic",
    "Beast",
    "Bird",
    "Bug",
    "Others",
    "Plant",
    "Reptile"
  ],
  Collections: async collectionName => {
    const opts = {
      Aquatic,
      Beast,
      Bird,
      Bug,
      Plant,
      Reptile
    };

    return await (opts[collectionName] || Others);
  }
};
