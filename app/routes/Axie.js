"use strict";

module.exports = {
  Landing: async (req, res) => {
    try {
      res.status(200).json({
        status: "success"
      });
    } catch (error) {
      res.status(500).json({
        message: "Something went wrong",
        status: "error"
      });
    }
  }
};
