"use strict";

const Mongoose = require("mongoose");

const BirdSchema = new Mongoose.Schema({
  axieId: {
    required: true,
    type: String,
    unique: true
  },
  class: {
    required: true,
    type: String
  },
  name: {
    required: true,
    type: String
  },
  price: {
    required: true,
    type: String
  },
  stage: {
    required: true,
    type: Number
  }
});

module.exports = Mongoose.model("Bird", BirdSchema);
